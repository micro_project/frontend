import { Component, HostListener, inject } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { LocalStorageService } from "./core/services/local-storage.service";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet
  ],
  template: '<router-outlet></router-outlet>',
})
export class AppComponent {
  private localStorageService: LocalStorageService = inject(LocalStorageService);
  @HostListener('window:unload', ['$event'])
  unloadHandler(event: any) {
    this.localStorageService.removeItem('authManagerAccountAppToken');
    this.localStorageService.removeItem('currentUserManagerAccountAppToken');
  }
}

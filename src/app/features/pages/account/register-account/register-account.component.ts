import {Component, inject, OnInit} from '@angular/core';
import {PositionService} from "../../../../core/services/position.service";
import {PositionsDto} from "../../../../core/DTO/positions-dto";
import {CommonModule} from "@angular/common";
import {
  FormBuilder,
  FormGroup,
  FormGroupDirective,
  ReactiveFormsModule,
  Validators
} from "@angular/forms";
import {AccountService} from "../../../../core/services/account.service";
import {Router} from '@angular/router';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-register-account',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  templateUrl: './register-account.component.html',
  styleUrl: './register-account.component.scss'
})
export class RegisterAccountComponent implements OnInit {
  private positionService: PositionService = inject(PositionService);
  private formBuilder: FormBuilder = inject(FormBuilder);
  private accountService: AccountService = inject(AccountService);
  private router: Router = inject(Router);
  private toastr: ToastrService = inject(ToastrService);

  getAllPositions: PositionsDto[] = [];
  getSubPositions: any[] = [];

  formData: FormGroup = this.formBuilder.group({
    name: [null, Validators.required],
    surname: [null, Validators.required],
    email: [null, [Validators.required, Validators.email]],
    description: [""],
    positionName: [null, Validators.required],
    answers: this.formBuilder.array([
      this.formBuilder.group( {
        name: null,
        inputType: 'text',
      }),
      this.formBuilder.group( {
        name: null,
        inputType: 'text',
      }),
      this.formBuilder.group( {
        name: null,
        inputType: 'checkbox',
      }),
    ])
  });

  ngOnInit(): void {
    this.positionService.getAllPositions().subscribe(value => this.getAllPositions = value)
  }

  getPosition(data: PositionsDto) {
    this.getSubPositions = data.subPosition
  }

  createUser(form: FormGroupDirective) {
    this.accountService.registerNewUser(form.form.value).subscribe({
      next: (result) => {
        if (result.detail) {
          this.toastr.error(result.detail, 'Błąd po stronie serwera', {
            timeOut: 10000,
            positionClass: 'toast-top-left',
          });
        } else {
          this.router.navigate([`/login`])
        }
      },
      error: (err) => this.toastr.error(err.error.detail, 'Błąd po stronie serwera', {
        timeOut: 10000,
        positionClass: 'toast-top-left',
      }),
      complete: () => this.toastr.success(`Sprawdż swoją skrzynkę pocztową z mailem oraz hasłem do zalogowania się do naszego systemu`, 'Rejestracja przebiegła pomyślnie', {
        timeOut: 10000,
        positionClass: 'toast-top-left',
      })
    })
  }
}

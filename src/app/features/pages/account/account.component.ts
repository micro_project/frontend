import { Component } from '@angular/core';
import {RouterOutlet} from "@angular/router";
import {HeaderComponent} from "../../../core/layout/header/header.component";

@Component({
  selector: 'app-account',
  standalone: true,
  imports: [
    RouterOutlet,
    HeaderComponent
  ],
  template: '<app-header></app-header><router-outlet></router-outlet>',
})
export class AccountComponent {

}

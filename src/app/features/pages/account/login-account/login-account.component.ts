import { Component, inject } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormControl, FormGroup, FormGroupDirective, ReactiveFormsModule, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { LoginService } from "../../../../core/services/login.service";
import { LocalStorageService } from "../../../../core/services/local-storage.service";

@Component({
  selector: 'app-login-account',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  templateUrl: './login-account.component.html',
  styleUrl: './login-account.component.scss'
})
export class LoginAccountComponent {
  formData = new FormGroup({
    email: new FormControl("", [Validators.required, Validators.email]),
    password: new FormControl("", [Validators.required]),
  });
  private loginService: LoginService = inject(LoginService);
  private localStorageService: LocalStorageService = inject(LocalStorageService);
  private router: Router = inject(Router);
  private toastr: ToastrService = inject(ToastrService);

  currentUser(email: string) {
    this.loginService.afterLoginShowCurrentUser(email).subscribe(value => this.localStorageService.setItem('currentUserManagerAccountAppToken', JSON.stringify(value)));
  }

  loginUser(form: FormGroupDirective) {
    this.loginService.loginToApplication(form.form.value).subscribe({
      next: value => {
        this.localStorageService.setItem('authManagerAccountAppToken', JSON.stringify(value));

        return this.currentUser(form.form.value.email);
      },
      error: (err) => this.toastr.error(err.error.detail, 'Błąd po stronie serwera lub logowania sprawdż swoje dane jeszcze raz i wpisz poprawnie', {
        timeOut: 10000,
        positionClass: 'toast-top-left',
      }),
      complete: () => {
        this.toastr.success(`Sprawdż swoją skrzynkę pocztową z mailem oraz hasłem do zalogowania się do naszego systemu`, 'Rejestracja przebiegła pomyślnie', {
          timeOut: 10000,
          positionClass: 'toast-top-left',
        });
        this.router.navigate(['/admin'], {
          onSameUrlNavigation: "ignore",
        });
      }
    })
  }
}

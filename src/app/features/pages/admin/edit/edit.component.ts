import {Component, inject, OnInit} from '@angular/core';
import {CommonModule} from "@angular/common";
import {ActivatedRoute, Router, RouterLink} from "@angular/router";
import {FormBuilder, FormGroup, FormGroupDirective, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {PositionService} from "../../../../core/services/position.service";
import {AccountService} from "../../../../core/services/account.service";
import {ToastrService} from "ngx-toastr";
import {PositionsDto} from "../../../../core/DTO/positions-dto";

@Component({
  selector: 'app-edit',
  standalone: true,
  imports: [
    CommonModule,
    RouterLink,
    FormsModule,
    ReactiveFormsModule,
  ],
  templateUrl: './edit.component.html',
  styleUrl: './edit.component.scss'
})
export class EditComponent implements OnInit {
  private positionService: PositionService = inject(PositionService);
  private activatedRoute: ActivatedRoute = inject(ActivatedRoute);
  private formBuilder: FormBuilder = inject(FormBuilder);
  private accountService: AccountService = inject(AccountService);
  private router: Router = inject(Router);
  private toastr: ToastrService = inject(ToastrService);

  getAllPositions: PositionsDto[] = [];
  getSubPositions: any[] = [];
  getId = this.activatedRoute.snapshot.params['id'];
  formData!: FormGroup;

  ngOnInit(): void {
    this.positionService.getAllPositions().subscribe(value => this.getAllPositions = value)
    this.accountService.getUser(this.getId).subscribe(getCurrentUser => {

      this.formData = this.formBuilder.group({
        name: [getCurrentUser.name, Validators.required],
        surname: [getCurrentUser.surname, Validators.required],
        description: [getCurrentUser.description],
        positionName: [getCurrentUser.positions.name, Validators.required],
        answers: this.formBuilder.array([
          this.formBuilder.group({
            name: getCurrentUser.dataPosition[0].name,
            inputType: getCurrentUser.dataPosition[0].inputType
          }),
          this.formBuilder.group({
            name: getCurrentUser.dataPosition[1].name,
            inputType: getCurrentUser.dataPosition[1].inputType
          }),
          this.formBuilder.group({
            name: getCurrentUser.dataPosition[2].name,
            inputType: getCurrentUser.dataPosition[2].inputType
          }),
        ])
      });
    })
  }

  getPosition(data: PositionsDto) {
    this.getSubPositions = data.subPosition;
  }

  updateUser(form: FormGroupDirective) {
    this.accountService.updateUser(this.getId, form.form.value).subscribe({
      next: (result) => {
        if (result.detail) {
          this.toastr.error(result.detail, 'Błąd po stronie serwera', {
            timeOut: 10000,
            positionClass: 'toast-top-left',
          });
        } else {
          this.router.navigate([`/admin`])
        }
      },
      error: (err) => this.toastr.error(err.error.detail, 'Błąd po stronie serwera', {
        timeOut: 10000,
        positionClass: 'toast-top-left',
      }),
      complete: () => this.toastr.success(`Dane zostały zaktualizowane na serwerze`, 'Edycja przebiegła pomyślnie', {
        timeOut: 10000,
        positionClass: 'toast-top-left',
      })
    })
  }
}

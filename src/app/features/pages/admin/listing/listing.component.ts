import {Component, inject, OnInit} from '@angular/core';
import {CommonModule} from "@angular/common";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {AccountService} from "../../../../core/services/account.service";
import {ListingUsersDto} from "../../../../core/DTO/listing-users-dto";
import {ToastrService} from "ngx-toastr";
import { RouterLink } from "@angular/router";

@Component({
  selector: 'app-listing',
  standalone: true,
  imports: [
    CommonModule,
    NgxDatatableModule,
    RouterLink,
  ],
  templateUrl: './listing.component.html',
  styleUrl: './listing.component.scss'
})
export class ListingComponent implements OnInit {
  private accountService: AccountService = inject(AccountService);
  private toastr: ToastrService = inject(ToastrService);

  getAllAccounts: ListingUsersDto[] = []

  ngOnInit(): void {
    this.listingUsers()
  }

  listingUsers(){
    this.accountService.getAllUsers().subscribe(value => this.getAllAccounts = value)
  }

  removeAccount(row: ListingUsersDto) {
    let currentUserManagerAccountAppToken = localStorage.getItem('currentUserManagerAccountAppToken');

    if (currentUserManagerAccountAppToken) {
      let getCurrentUser = JSON.parse(currentUserManagerAccountAppToken);
      if (getCurrentUser.roles[0] === 'ROLE_USER') {
        this.toastr.error("Nie masz uprawnień do wykonywania tej akcji", 'Ważna informacja', {
          timeOut: 10000,
          positionClass: 'toast-top-left',
        });
      }
      this.accountService.removeUser(row.id).subscribe(value =>  this.listingUsers())
    }
  }
}

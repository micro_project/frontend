
export interface RegisterUserDto  <T> {
  name: string,
  surname: string,
  email: string,
  password: string,
  description: string,
  positionName: string,
  answers: T[]
}

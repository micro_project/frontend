export interface UpdateUserDto <T> {
  name: string,
  surname: string,
  description: string,
  positionName: string,
  answers: T[]
}

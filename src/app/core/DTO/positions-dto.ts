export interface PositionsDto {
  name: string,
  subPosition: string[],
}

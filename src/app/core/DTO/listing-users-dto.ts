export interface ListingUsersDto {
  id: number,
  name: string,
  surname: string,
  email: string,
  password: string,
  description: string,
  positionName: string,
  roles: string[]
  answers: any[]
}

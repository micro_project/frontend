import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {RegisterUserDto} from "../DTO/register-user-dto";
import {UpdateUserDto} from "../DTO/update-user-dto";
import {DataAnswer} from "../DTO/DataAnswer-dto";

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  constructor(private http: HttpClient) {
  }

  getAllUsers(): Observable<any> {
    return this.http.get(`${environment.url}/users`);
  }

  registerNewUser(data: RegisterUserDto<DataAnswer>): Observable<any> {
    return this.http.post(`${environment.url}/register`, data);
  }

  updateUser(id: number, data: UpdateUserDto<DataAnswer>): Observable<any> {
    return this.http.put(`${environment.url}/update/${id}`, data);
  }

  getUser(id: number): Observable<any> {
    return this.http.get(`${environment.url}/user/${id}`);
  }

  removeUser(id: number): Observable<any> {
    return this.http.delete(`${environment.url}/remove/${id}`);
  }
}

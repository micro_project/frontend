import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {LoginDto} from "../DTO/login-dto";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) {
  }

  loginToApplication(data: LoginDto): Observable<any> {
    return this.http.post(`${environment.url}/login_check`, {
      username: data.email,
      password: data.password,
    });
  }

  afterLoginShowCurrentUser(email: string): Observable<any> {
    return this.http.post(`${environment.url}/get_email`, {
      email: email,
    });
  }
}

import { ChangeDetectionStrategy, Component, inject, OnInit } from '@angular/core';
import { Router, RouterLink } from "@angular/router";
import { CommonModule } from "@angular/common";
import { LocalStorageService } from "../../services/local-storage.service";

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [
    CommonModule,
    RouterLink
  ],
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class HeaderComponent implements OnInit {
  getCurrentUser!: any;
  private router: Router = inject(Router);
  private localStorageService: LocalStorageService = inject(LocalStorageService);

  ngOnInit(): void {
    let currentUserManagerAccountAppToken = this.localStorageService.getItem('currentUserManagerAccountAppToken');

    if (currentUserManagerAccountAppToken) {
      this.getCurrentUser = JSON.parse(currentUserManagerAccountAppToken);
    }
  }

  logoutUser() {
    this.localStorageService.removeItem('authManagerAccountAppToken');
    this.localStorageService.removeItem('currentUserManagerAccountAppToken');

    this.router.navigate([``], {
      onSameUrlNavigation: "ignore"
    })
  }
}

import { HttpInterceptorFn } from '@angular/common/http';

export const authInterceptor: HttpInterceptorFn = (req, next) => {
  let request = req.clone();
  let token: any = localStorage.getItem('authManagerAccountAppToken');

  if (token) {
    let tokenData = JSON.parse(token);

    request = req.clone({
      setHeaders: {
        Authorization: `Bearer ${tokenData.token}`
      },
    });

    return next(request);
  }

  return next(request);
};

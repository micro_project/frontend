import { Routes } from '@angular/router';
import {AccountComponent} from "./features/pages/account/account.component";
import {AdminComponent} from "./features/pages/admin/admin.component";
import {PageNotFoundComponent} from "./core/page-not-found/page-not-found.component";
import {AfterLoggedUserAuthGuard} from "./core/guards/after-logged-user-auth.guard";

export const routes: Routes = [
  {
    path: '',
    component: AccountComponent,
    children: [
      {
        path: '',
        loadComponent: () => import('./features/pages/account/register-account/register-account.component')
          .then(module => module.RegisterAccountComponent)
      },
      {
        path: 'login',
        loadComponent: () => import('./features/pages/account/login-account/login-account.component')
          .then(module => module.LoginAccountComponent)
      },
    ]
  },
  {
    path: 'admin',
    canActivate: [AfterLoggedUserAuthGuard],
    component: AdminComponent,
    children: [
      {
        path: '',
        loadComponent: () => import('./features/pages/admin/listing/listing.component')
          .then(module => module.ListingComponent)
      },
      {
        path: ':id',
        loadComponent: () => import('./features/pages/admin/edit/edit.component')
          .then(module => module.EditComponent)
      },
    ]
  },

  {path: '', redirectTo: '/#/', pathMatch: 'full'},
  {path: '**', component: PageNotFoundComponent}
];
